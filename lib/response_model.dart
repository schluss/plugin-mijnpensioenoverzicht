/**
 * @author Asanka Anthony
 * @email aanthony@yukon.lk
 * @create date 2020-06-04 14:32:23
 * @modify date 2020-06-04 14:32:23
 * @desc response data model when calling extracted data function 
 */

class ResponseModel {
  String attributeName;
  String displayName;
  bool isVisible; // manage visibility in UI
  String value; // actual data from belasting xml
  String searchId; // find attribute from xml
  String type; // String.Int or List
  List<String> listOfAttributes; // List of child names we need to set in list
  Function callback; // callable function if needed customization
  ResponseModel({
    this.attributeName,
    this.displayName,
    this.isVisible,
    this.value,
    this.searchId,
    this.type,
    this.callback,
  });

  List<String> get getListOfAttributes => listOfAttributes;

  set setListOfAttributes(List<String> listOfAttributes) {
    listOfAttributes = listOfAttributes;
  }

  String get getAttributeName => attributeName;

  set setAttributeName(String attributeName) => this.attributeName = attributeName;
  String get getType => type;

  set setType(String searchId) => this.type = type;

  String get getSearchId => searchId;

  set setSearchId(String searchId) => this.searchId = searchId;

  String get getDisplayName => displayName;

  set setDisplayName(String displayName) => this.displayName = displayName;

  bool get getIsVisible => isVisible;

  set setIsVisible(bool isVisible) => this.isVisible = isVisible;

  String get getValue => value;

  set setValue(String value) => this.value = value;

  @override
  String toString() {
    return 'ResponseModel{attributeName: $attributeName, displayName: $displayName, isVisible: $isVisible, value: $value}';
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is ResponseModel && runtimeType == other.runtimeType && attributeName == other.attributeName && displayName == other.displayName && isVisible == other.isVisible && value == other.value;

  @override
  int get hashCode => attributeName.hashCode ^ displayName.hashCode ^ isVisible.hashCode ^ value.hashCode;
}
