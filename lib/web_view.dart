import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class BrowserWebView extends StatelessWidget {
  final url;
  const BrowserWebView({Key key,this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: url,
      displayZoomControls: true,
      withZoom: true,
    );
  }
}
