import 'package:logger/logger.dart';
import 'package:plugin_mijnpensioenoverzicht/response_model.dart';
import 'package:xml_parser/xml_parser.dart';

class Template {
  // manage expected list of attributes

  Map<String, ResponseModel> expectedFields() => {
        'Belastingjaar': ResponseModel(attributeName: 'taxyear', displayName: 'Belastingjaar', isVisible: true, searchId: "Belastingjaar", type: "Int"),

        'Watermerk': ResponseModel(attributeName: 'watermark', displayName: 'Watermerk', isVisible: true, searchId: "Watermerk", type: "String"),

        'Initialen': ResponseModel(
            //First part of value until last occuring space, for example
            //'J' or 'J D'. Then add a dot '.' after every letter to it so it becomes 'J.' or 'J. D.'
            attributeName: 'initials',
            displayName: 'Initialen',
            isVisible: true,
            searchId: "NaamAangever",
            callback: getInitials,
            type: "String"),

        'Achternaam': ResponseModel(
            //Last part of value after space, for example 'JANSSEN'.
            //Then uppercase first and lowercase the rest, like 'Janssen'.
            attributeName: 'lastname',
            displayName: 'Achternaam',
            isVisible: true,
            searchId: "NaamAangever",
            callback: getLastName,
            type: "String"),

        'GebDatumAangever': ResponseModel(
            //Date format YYYY-MM-DD. Must be stored like DD-MM-YYYY
            attributeName: 'birthdate',
            displayName: 'GebDatumAangever',
            isVisible: true,
            searchId: "GebDatumAangever",
            type: "String"),

        'Bankrekeningnummer': ResponseModel(
          //Date format YYYY-MM-DD. Must be stored like DD-MM-YYYY
          attributeName: 'birthdate',
          displayName: 'Bankrekeningnummer',
          isVisible: true,
          searchId: "BankrekNrBRGAangever",
          type: "String",
        ),

//OLD FIELDS
        'DagtekeningVA': ResponseModel(
          attributeName: 'bankaccountnumber',
          displayName: 'DagtekeningVA',
          isVisible: true,
          searchId: "DagtekeningVA",
        ),
      };
}

var logger = Logger();

// get initials from full name
String getInitials(String fullName) {
  String text = fullName;
  List<String> byspace = text.split(" ");
  byspace.removeLast(); // remove last name
  return (byspace.join("."));
}

String getLastName(String fullName) {
  String text = fullName;
  List<String> byspace = text.split(" ");
  return byspace.last; // get last name
}

String getChildValue(XmlElement element, String childId) {
  String result = "";
  try {
    return element.getChild(childId).text; //
  } catch (e) {
    logger.w(e);
    return result;
  }
}
