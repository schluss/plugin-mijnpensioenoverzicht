import 'package:flutter/material.dart';
import 'package:plugin_mijnpensioenoverzicht/mijnpensioenoverzicht.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  void _mainAppExpected(String filePath, BuildContext context) {
    PluginMijnpensioenoverzicht().getExtractedData(filePath).then((onValue) {});
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        '/': (_) => MyHomePage(title: 'Flutter WebView Demo'),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          //PluginBelasting().runPlugin(context,_mainAppExpected); //call belasting plugin example showcase cant run in here
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
