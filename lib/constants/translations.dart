class TranslationsNL {
  static const String terugText = "Terug";
  static const String webViewConnectText1 = "Je gaat nu naar de website van ";
  static const String webViewConnectText2 = " om in te loggen";
  static const String waitingText = "Pagina’s verwerken, een momentje alsjeblieft";
}

class TranslationsEN {
  static const String terugText = "Back";
  static const String webViewConnectText1 = "Je gaat nu naar de website van ";
  static const String webViewConnectText2 = " om in te loggen";
  static const String waitingText = "Processing, please wait";
}

class TranslationsUK {
  static const String terugText = "Назад";
  static const String webViewConnectText1 = "Je gaat nu naar de website van ";
  static const String webViewConnectText2 = " om in te loggen";
  static const String waitingText = "Processing, please wait";
}
