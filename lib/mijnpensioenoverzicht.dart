library plugin_mijnpensioenoverzicht;

import 'dart:io';

import 'package:plugin_mijnpensioenoverzicht/response_model.dart';
import 'package:plugin_mijnpensioenoverzicht/template.dart';
import 'package:plugin_mijnpensioenoverzicht/data_web_view_page.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:xml_parser/xml_parser.dart';

class PluginMijnpensioenoverzicht {
  Logger _logger = Logger();
  runPlugin(context, Function callBack, String siteUrl, String trigger,
      {String pluginName = "Mijnpensioenoverzicht"}) {
    Locale locale = Localizations.localeOf(context);
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => DataWebViewPage(
                callBack,
                siteUrl,
                trigger,
                locale,
                pluginName: pluginName,
              )),
    );
  }

//Extract data from xml file
  Future<List<ResponseModel>> getExtractedData(String filePath) async {
    List<ResponseModel> list = List(); // create response
    //String xmlStr = await rootBundle.loadString(filePath);
    File file = new File(filePath); // get file content to object
    String xmlStr = await file.readAsString();
    XmlDocument xmlDocument = XmlDocument.fromString(xmlStr);
    Template().expectedFields().forEach((k, v) {
      // Loop all expected list of attributes
      String val = "";

      if (v.type == "List") {
        List<XmlElement> xmlElementAvailList = xmlDocument
            .getElements(v.getSearchId); // get list of chidren elements
        if (xmlElementAvailList != null) {
          val = v.callback(xmlElementAvailList);
        }
      } else {
        XmlElement xmlElementAvail = xmlDocument
            .getLastElement(v.getSearchId); // get attribute value from xml
        if (xmlElementAvail != null) {
          // If element found
          val = xmlElementAvail.text;
        }
        if (v.callback != null && val != "")
          val = v.callback(
              val); // if callback function defined ? if yes call that function with extract string value
      }

      // Printing Log messages:
      (val == "" || val == null) ? _logger.w("${v.attributeName}: Not found") : _logger.i("${v.attributeName}: Found");

      ResponseModel model = ResponseModel(
        //crate response object
        attributeName: v.attributeName,
        displayName: v.displayName,
        isVisible: v.getIsVisible,
        value: val, //set attribute value
      );
      list.add(model);
    });
    if (await fileDelete(filePath)) _logger.d("File deleted");
    return Future.value(list);
  }

  Future<bool> fileDelete(String filePath) async {
    if (await checkFileExit(filePath)) {
      final dir = Directory(filePath);
      dir.deleteSync(recursive: true);
      bool isDeleted = !await checkFileExit(filePath);
      if (isDeleted) {
        return true;
      }
    }
    return false;
  }

  Future<bool> checkFileExit(String filePath) async {
    if (await File(filePath).exists()) {
      _logger.d("File exists");
      return true;
    } else {
      _logger.d("File don't exists");
      return false;
    }
  }
}
