import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:plugin_mijnpensioenoverzicht/constants/ui_constants.dart';

/*
 * preview until page loaing 
 */
class PageLoadingView extends StatelessWidget {
  final Locale locale;
  final String text;
  const PageLoadingView(this.locale, {this.text});

  //final provider = "Belastingdienst";
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          text,
          style: TextStyle(color: UIConstants.mediumGrey, fontSize: width * 0.05),
          textAlign: TextAlign.center,
        ),
        Padding(
          padding: EdgeInsets.only(top: height * 0.02),
          child: Container(
            height: 16,
            child: SpinKitThreeBounce(
              color: UIConstants.paleLilac,
              size: 16,
            ),
          ),
        )
      ],
    );
  }
}
