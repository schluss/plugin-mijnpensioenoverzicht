/*
 * @author Asanka Anthony
 * @email aanthony@yukon.lk
 * @create date 2020-09-16 23:45:29
 * @modify date 2020-09-16 23:45:29
 * @desc inspired from progress_dialog: ^1.2.4 library 
 */

import 'package:plugin_mijnpensioenoverzicht/constants/ui_constants.dart';
import 'package:plugin_mijnpensioenoverzicht/page_loading.dart';
import 'package:plugin_mijnpensioenoverzicht/reused_widgets/back_arraw_text.dart';
import 'package:plugin_mijnpensioenoverzicht/reused_widgets/page_header.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

String _dialogMessage =
    "Verwerken..."; // initial loading text if there is any delay to call
Locale _locale; // use for language text

bool _isShowing = false; // dialog is showing or not
BuildContext _context, _dismissingContext; // open & close context
bool _barrierDismissible = true, _showLogs = false; // properties
Function _closeOrBack; // call back function for close / back button

class SchlussProgress {
  _Body _dialog; // dialog boday container

  SchlussProgress(
    BuildContext context,
    Locale locale,
    Function closeOrBack, {
    bool isDismissible,
    bool showLogs,
  }) {
    _context = context;
    _barrierDismissible = isDismissible ?? true;
    _showLogs = showLogs ?? false;
    _locale = locale;
    _closeOrBack = closeOrBack;
  }
  // update waiting screen url text
  void update({
    String message,
  }) {
    _dialogMessage = message ?? _dialogMessage;

    if (_isShowing) _dialog.update();
  }

  //check dialog is open or not
  bool isShowing() {
    return _isShowing;
  }

  // hide dialog box
  Future<bool> hide() async {
    try {
      if (_isShowing) {
        _isShowing = false;
        Navigator.of(_dismissingContext).pop(); // close dialog screen
        if (_showLogs) debugPrint('ProgressDialog dismissed');
        return Future.value(true);
      } else {
        if (_showLogs) debugPrint('ProgressDialog already dismissed');
        return Future.value(false);
      }
    } catch (err) {
      debugPrint('Seems there is an issue hiding dialog');
      debugPrint(err.toString());
      return Future.value(false);
    }
  }

  // open dialog screen
  Future<bool> show() async {
    try {
      if (!_isShowing) {
        _dialog = new _Body();
        showDialog<dynamic>(
          context: _context,
          barrierDismissible: _barrierDismissible,
          builder: (BuildContext context) {
            _dismissingContext = context; // keep context for use in hide
            return WillPopScope(
              onWillPop: () async => _barrierDismissible,
              child: Dialog(
                  child: _dialog,
                  insetPadding:
                      EdgeInsets.zero), // Remove paddings & set to full screen
            );
          },
        );
        // Delaying the function for 200 milliseconds
        // [Default transitionDuration of DialogRoute]
        await Future.delayed(Duration(milliseconds: 200));
        if (_showLogs) debugPrint('ProgressDialog shown');
        _isShowing = true;
        return true;
      } else {
        if (_showLogs) debugPrint("ProgressDialog already shown/showing");
        return false;
      }
    } catch (err) {
      _isShowing = false;
      debugPrint('Exception while showing the dialog');
      debugPrint(err.toString());
      return false;
    }
  }
}

// ignore: must_be_immutable
// use to keep connection between two classes
class _Body extends StatefulWidget {
  _BodyState _dialog = _BodyState();

  update() {
    _dialog.update();
  }

  @override
  State<StatefulWidget> createState() {
    return _dialog;
  }
}

class _BodyState extends State<_Body> {
  update() {
    setState(() {});
  }

  @override
  void dispose() {
    _isShowing = false;
    if (_showLogs) debugPrint('ProgressDialog dismissed by back button');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    _dismissingContext = context;

    return StatefulBuilder(
      builder: (context, setState) {
        return WillPopScope(
          onWillPop: () async => true,
          child: Scaffold(
            backgroundColor: Colors.white,
            body: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).padding.top + height * 0.1),
                  height: height,
                  width: width,
                  color: UIConstants.primaryColor,
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: width * 0.07,
                        right: width * 0.07,
                        bottom: height * 0.25),
                    child: PageLoadingView(_locale, text: _dialogMessage),
                  ),
                ),
                Stack(
                  children: <Widget>[
                    Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          height: height * 0.1,
                          color: UIConstants.paleLilac,
                        )),
                    Padding(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).padding.top),
                      child: PageHeader(
                        "",
                        backBtn,
                        _locale,
                        titleColor: UIConstants.accentColor,
                        closeBtnAction: backBtn, // set back button action
                        isDividerVisible: true,
                        backArrowText: BackArrowText(
                            UIConstants.mediumGrey, backBtn, _locale),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  /*back button trigger */
  void backBtn() {
    _closeOrBack(); //callback to main close & back button functions
  }
}
