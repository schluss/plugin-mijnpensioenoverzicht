import 'package:plugin_mijnpensioenoverzicht/constants/translations.dart';
import 'package:flutter/material.dart';

class TranslationsUtil {
  static final defaultLocale = "nl";
  static final en = "en";
  static final uk = "uk";

  static String getBackText(Locale locale) {
    String text = TranslationsNL.terugText;
    if (locale != null) {
      if (locale.languageCode == en) {
        text = TranslationsEN.terugText;
      } else if (locale.languageCode == uk) {
        text = TranslationsUK.terugText;
      }
    }
    return text;
  }

  static String webViewConnectText1(Locale locale) {
    String text = TranslationsNL.webViewConnectText1;
    if (locale != null) {
      if (locale.languageCode == en) {
        text = TranslationsEN.webViewConnectText1;
      } else if (locale.languageCode == uk) {
        text = TranslationsUK.webViewConnectText1;
      }
    }
    return text;
  }

  static String webViewConnectText2(Locale locale) {
    String text = TranslationsNL.webViewConnectText2;
    if (locale != null) {
      if (locale.languageCode == en) {
        text = TranslationsEN.webViewConnectText2;
      } else if (locale.languageCode == uk) {
        text = TranslationsUK.webViewConnectText2;
      }
    }
    return text;
  }

  static String getWaitingText(Locale locale) {
    String text = TranslationsNL.waitingText;
    if (locale != null) {
      if (locale.languageCode == en) {
        text = TranslationsEN.waitingText;
      } else if (locale.languageCode == uk) {
        text = TranslationsUK.waitingText;
      }
    }
    return text;
  }
}
