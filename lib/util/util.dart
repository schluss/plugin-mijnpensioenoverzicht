import 'dart:io';

import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';

import 'package:plugin_mijnpensioenoverzicht/response_model.dart';
import 'package:plugin_mijnpensioenoverzicht/read_xml.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart' as inApp;

import 'package:requests/requests.dart';

class Util {
  Logger _logger = Logger();

  final String _fileName = "belasting.xml";

  // File downloading and saving:
  Future<String> downloadAndSaveFile(
      String downloadUrl, List<inApp.Cookie> cookies) async {
      // Make a Hashmap and fill it with all needed cookies
      Map<String, String> cookiesMap = {};

      cookies.forEach((element) {
        cookiesMap[element.name] = element.value;
      });

      cookiesMap['testCookie'] = '/VIADownload2020';

      // Prepare request
      String hostname = Requests.getHostname(downloadUrl);
      await Requests.setStoredCookies(hostname, cookiesMap);

      // Do the request and get the response
      var response = await Requests.get(downloadUrl);

      dynamic body = response.content();

      // Store result as a file
      String _savePath =
          await getSavePath(_fileName); // Getting file saving location.

      File f = File(_savePath);
      f.writeAsString(body);

      return _savePath;
   
  }

  // Call to XML reader:
  Future<ResponseModel> executeXmlReader() async {
    String _savePath =
        await getSavePath(_fileName); // Getting file saving loaction.

    XMLReader _xmlReader = XMLReader();
    Future<ResponseModel> responseModel =
        _xmlReader.getExtractedData(_savePath);
    _logger.i(responseModel);
  }

  // Geting downloaded file saving path:
  Future<String> getSavePath(String fileName) async {
    var _dir =
        await getApplicationDocumentsDirectory(); // Getting downloaded files saving loaction.
    String _savePath =
        "${_dir.path}/$fileName"; // Setting downloaded file saving loaction.

    return _savePath; // Returning downloaded file saving path.
  }
}
