import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:logger/logger.dart';
import 'package:plugin_mijnpensioenoverzicht/util/Schluss_Progress.dart';
import 'package:plugin_mijnpensioenoverzicht/util/util.dart';

Logger _logger = Logger();
Locale _local;
SchlussProgress waiter; // progress screen instence

class WebViewBody extends StatefulWidget {
  final Function webViewCreatedCallback; // use to call after webview created
  final String initialUrl; // initial url to open
  final Function urlChangedCallback; // use to call after webview created
  final Function mainAppCallBack; // main application callback function
  final String trigger;
  final Locale local;
  WebViewBody(
    this.webViewCreatedCallback,
    this.initialUrl,
    this.urlChangedCallback,
    this.mainAppCallBack,
    this.trigger,
    this.local,
  ) {
    _local = local;
  }

  @override
  _WebViewBodyState createState() => _WebViewBodyState();
}

class _WebViewBodyState extends State<WebViewBody> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    waiter = SchlussProgress(
      context,
      _local, // Required for headers and text components
      closeOrBack, // function to call back when user click
    );
    // String tempURL = "https://stackoverflow.com/questions/54672230/";
    final CookieManager _cookieManager = CookieManager.instance();

    @override
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Container(
      height: height * 0.9,
      width: width,
      child: Column(
        children: <Widget>[
          Expanded(
            child: InAppWebView(
              initialUrl: widget.initialUrl,
              initialHeaders: {},
              initialOptions: InAppWebViewGroupOptions(
                  android: AndroidInAppWebViewOptions(useHybridComposition: true),
                  crossPlatform: InAppWebViewOptions(
                debuggingEnabled: true,
                useOnDownloadStart: true,
                useShouldOverrideUrlLoading: false,
                cacheEnabled: false, //chnage to true or false to enable or disable cache
                clearCache: true, //Clears all the webview's cache.
              )),
              onWebViewCreated: (InAppWebViewController appWebViewController) async {
                if (!waiter.isShowing()) {
                  await waiter.show();
                }
                waiter.update(message: "Verwerken... \n");

                widget.webViewCreatedCallback(appWebViewController); //call given function after webview created
              },
              onLoadStart: (controller, url) async {
                // create a nice formatted url (https://site.com/...page.html)
                String r = url;
                if (r.length > 45) {
                  r = r.substring(0, 32) + '...' + r.substring(r.length - 10);
                }
                await waiter.show();
                waiter.update(message: "Verwerken... \n" + r);
                // showing waiter should be here, at the start of loading, unfortinately in release mode .hide(), does'nt work anymore
                if (!waiter.isShowing()) {
                  await waiter.show();
                }
              },
              onDownloadStart: (InAppWebViewController controller, String url) async {
                String downloadValue = await downloadTrigger(controller, url, _cookieManager);
                // hide the waiter after download again:
                await waiter.hide();
                widget.mainAppCallBack(downloadValue, context);
              },
              onLoadStop: (InAppWebViewController controller, String url) async {
                _logger.i("loaded url: " + url);

                // Show waiter dialog, and only hide it at specific pages (where user input is needed)
                if (url == "https://digid.nl/inloggen" || url == "https://digid.nl/sms_controleren" || url == "https://digid.nl/bevestig_telefoonnummer") {
                  await waiter.hide();
                } else if (url.contains("services.schluss.app/beta_belastingdienst")) {
                  // Demo site auto redirecting
                  await waiter.hide();
                }

                // automatically make 'self' selection and continue to login entry page
                if (url == "https://mijn.belastingdienst.nl/VIADownload2020/index.html") {
                  String js = "window.setTimeout(function(){document.getElementById('radioA1').checked = true; document.querySelector('#submit').click()},0);";
                  controller.evaluateJavascript(source: js);
                }
                if (url.contains("schluss.app/beta_belastingdienst/belastingdienst.html")) {
                  String js = "window.setTimeout(function(){document.querySelector('#MainContent > div.panel > a ').click()},0);";
                  controller.evaluateJavascript(source: js);
                }

                // automatically select digid login method
                if (url == "https://mijn.belastingdienst.nl/GTService/#/inloggen") {
                  String js = "window.setTimeout(function(){document.querySelector('#a_digid').click()},0);";
                  controller.evaluateJavascript(source: js);
                }

                // na inloggen toon:
                // https://www.mijnpensioenoverzicht.nl/mijn-gegevens

                // dan wachten, formulier invullen en persoon klikt op knop 'Bekijk mijn pensioenoverzicht'
                // mogelijk hernoemen (Download mijn..), pad: document.querySelector("#dropdown-target > div > main > div > div.timeline-single-age > section > form > fieldset:nth-child(4) > button")

                // Na klikken op buttom, waiter tonen, kom je uit op:
                // https://www.mijnpensioenoverzicht.nl/mijn-pensioen-nu

                // dan automatisch doorgaan naar download:
                // DOWNLOAD URL IS:
                // https://www.mijnpensioenoverzicht.nl/xml

                // download page:
                //https://www.mijnpensioenoverzicht.nl/downloads-pensioenoverzicht#downloadxml

                // then click on link:
                // document.querySelector("#downloadxml > div > div > div > p > a")

                // then download starts

                // after login automatically start the download by clicking the download button. The download is then catched at 'onDownloadStart'
                if (url == "https://mijn.belastingdienst.nl/VIADownload2020/gegevensDownloaden.html") {
                  String js = "window.setTimeout(function(){document.querySelector('#MainContent > div.panel > form > a').click()},0);";
                  controller.evaluateJavascript(source: js);
                }
              },
            ),
          )
        ],
      ),
    );
  }

  // Callback function for close and back buttons
  void closeOrBack() {
    waiter.hide(); // hide waiting screen
    Navigator.pop(context); // call previous UI
  }
}

/*
 * download file & save 
 */
Future<String> downloadTrigger(InAppWebViewController controller, String url, CookieManager _cookieManager) async {
  List<Cookie> cookies = await _cookieManager.getCookies(url: url);
  _logger.i("onDownloadStart $url");
  Util _util = Util();
  String downloadValue = await _util.downloadAndSaveFile(url, cookies);
  return downloadValue;
}
