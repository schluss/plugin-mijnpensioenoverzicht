import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:plugin_mijnpensioenoverzicht/constants/ui_constants.dart';
import 'package:plugin_mijnpensioenoverzicht/page_loading.dart';
import 'package:plugin_mijnpensioenoverzicht/reused_widgets/back_arraw_text.dart';
import 'package:plugin_mijnpensioenoverzicht/reused_widgets/page_header.dart';
import 'package:plugin_mijnpensioenoverzicht/util/translations_util.dart';
import 'package:plugin_mijnpensioenoverzicht/web_view_body.dart';

class DataWebViewPage extends StatefulWidget {
  Function callBack; // function to callback main application
  String siteUrl;
  String trigger;
  String pluginName;
  final Locale locale;
  DataWebViewPage(this.callBack, this.siteUrl, this.trigger, this.locale, {this.pluginName});

  @override
  _DataWebViewPageState createState() => _DataWebViewPageState();
}

class _DataWebViewPageState extends State<DataWebViewPage> {
  //see discussion here: https://schluss.atlassian.net/browse/BETA-157
  var _url = 'https://services.schluss.app/beta_belastingdienstGG';
  bool _isLoading = true;
  // change this wariable to show waiting text
  bool _isWaiting = false;

  Timer _timer;
  _DataWebViewPageState() {
    // set timmer but not work with inappbrowser widget
    _timer = new Timer(const Duration(seconds: 3), () {
      setState(() {
        _isLoading = false;
      });
    });
  }
  InAppWebViewController _appWebViewController; // caontain webview controller object
  void _afterInAppWebCreated(InAppWebViewController webViewController) {
    _appWebViewController = webViewController;
  }

  void _urlChanged(url) {
    setState(() {
      _url = url; // set url change
    });
  }

  void _callBackMethod(String status, BuildContext context) {
    widget.callBack(status, context); //callback to main application
  }

  @override
  Widget build(BuildContext context) {
    _url = widget.siteUrl;
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var loadingText = TranslationsUtil.webViewConnectText1(widget.locale) + widget.pluginName + TranslationsUtil.webViewConnectText2(widget.locale);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top + height * 0.1),
            height: height,
            width: width,
            color: UIConstants.primaryColor,
            child: Container(
              height: height * 0.9,
              width: width,
              child: !_isLoading && !_isWaiting
                  ? WebViewBody(
                      _afterInAppWebCreated,
                      _url,
                      _urlChanged,
                      _callBackMethod,
                      widget.trigger,
                      widget.locale,
                    )
                  : Padding(
                      padding: EdgeInsets.only(left: width * 0.07, right: width * 0.07, bottom: height * 0.25),
                      child: PageLoadingView(
                        widget.locale,
                        text: _isLoading ? loadingText : TranslationsUtil.getWaitingText(widget.locale),
                      ),
                    ),
            ),
          ),
          Stack(
            children: <Widget>[
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: height * 0.1,
                    color: UIConstants.paleLilac,
                  )),
              Padding(
                padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                child: PageHeader(
                  _url,
                  backBtn,
                  widget.locale,
                  titleColor: UIConstants.accentColor,
                  iconPath: "assets/images/lock.png",
                  isDividerVisible: true,
                  backArrowText: BackArrowText(UIConstants.mediumGrey, backBtn, widget.locale),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  /*back button trigger */
  void backBtn() {
    _appWebViewController.goBack(); // call prev UI
  }
}
