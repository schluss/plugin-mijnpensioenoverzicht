import 'package:plugin_mijnpensioenoverzicht/constants/ui_constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CircleCloseButton extends StatelessWidget {
  final Color backgroundColor;
  final Color iconColor;
  final Function call;

  const CircleCloseButton(this.backgroundColor, this.iconColor, this.call);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        call();
      },
      child: Container(
        width: MediaQuery.of(context).size.width * 0.095,
        height: MediaQuery.of(context).size.width * 0.095,
        alignment: Alignment.center,
        decoration: new BoxDecoration(
            shape: BoxShape.circle,
            color: backgroundColor,
            border: Border.all(color: UIConstants.paleLilac)),
        child: Icon(
          Icons.close,
          color: iconColor,
          size: MediaQuery.of(context).size.width * 0.05,
        ),
      ),
    );
  }
}
