import 'package:flutter/material.dart';
import 'package:plugin_mijnpensioenoverzicht/util/translations_util.dart';

class BackArrowText extends StatelessWidget {
  final Color color;
  final Function backBtnFn; // trigger back button
  final Locale locale;
  Alignment alignment;
  BackArrowText(this.color, this.backBtnFn, this.locale, {this.alignment = Alignment.centerLeft});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: alignment,
      child: GestureDetector(
        onTap: () {
          backBtnFn(); // call back button functon
        },
        child: Row(
          children: <Widget>[
            Icon(
              Icons.keyboard_arrow_left,
              color: color,
              size: MediaQuery.of(context).size.width * 0.06,
            ),
            Text(
              TranslationsUtil.getBackText(locale),
              style: TextStyle(color: color, fontFamily: "Graphik", fontSize: MediaQuery.of(context).size.width * 0.042),
            ),
          ],
        ),
      ),
    );
  }
}
