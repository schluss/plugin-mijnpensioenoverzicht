import 'package:plugin_mijnpensioenoverzicht/reused_widgets/circle_close_button.dart';
import 'package:flutter/material.dart';
import 'package:plugin_mijnpensioenoverzicht/constants/ui_constants.dart';

import 'back_arraw_text.dart';

class PageHeader extends StatelessWidget {
  final String pageTitle;
  final BackArrowText backArrowText;
  final Color titleColor;
  final String iconPath;
  final bool isDividerVisible;
  final Function backBtnFunction;
  final Function closeBtnAction;
  final Locale locale;
  PageHeader(this.pageTitle, this.backBtnFunction, this.locale,
      {this.backArrowText,
      this.closeBtnAction,
      this.titleColor = UIConstants.headerTitleText,
      this.iconPath,
      this.isDividerVisible = false});

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Container(
      decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(20),
              topRight: const Radius.circular(20))),
      height: height * 0.1,
      alignment: Alignment.center,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: ConstrainedBox(
              constraints: new BoxConstraints(
                maxWidth: width * 0.615,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  (iconPath != null)
                      ? Image.asset(iconPath, height: width * 0.045)
                      : Container(),
                  Flexible(
                    child: Text(
                      pageTitle,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: titleColor,
                          fontSize: width * 0.04,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal),
                    ),
                  ),
                ],
              ),
            ),
          ),
          (backArrowText != null)
              ? Padding(
                  padding: EdgeInsets.only(left: height * 0.02),
                  child: BackArrowText(
                      UIConstants.mediumGrey, () => Navigator.pop(context), locale),
                )
              : Container(),
          Padding(
            padding: EdgeInsets.only(right: height * 0.02),
            child: Align(
              alignment: Alignment.centerRight,
              child: closeBtnAction == null
                  ? CircleCloseButton(Colors.white, UIConstants.accentColor,
                      () {
                      Navigator.pop(context);
                    })
                  : CircleCloseButton(Colors.white, UIConstants.accentColor,
                      callBackFromCloseBtn),
            ),
          ),
          (isDividerVisible)
              ? Align(
                  alignment: Alignment.bottomCenter,
                  child: Divider(height: 1),
                )
              : Container(),
        ],
      ),
    );
  }

  callBackFromCloseBtn() {
    closeBtnAction();
  }
}
